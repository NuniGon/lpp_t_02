require "question/simple_choice"

module Question
	class SimpleChoice
		describe Question::SimpleChoice do
			before :each do
				@q = Question::SimpleChoice.new(:text => '2+2',
								:right => 4,
								:distractor => [9,3,1] )
			end
			
			context "When building a question" do
			it "has to have a text and some question" do
			expect(@q.text)=='2+2='
			end
			end
		end
	end	
end

