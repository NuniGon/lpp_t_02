module Question
  class SimpleChoice
   attr_accessor :text , :right , :distractor

	@@count = 0
	def self.id
		@@count
	end

	def initialize(args)
		@text = args[:text]
			raise ArgumentError, ‘Specify :text , :right  and :distractor’ unless @text
		@right = args[:right]
			raise ArgumentError, ‘Specify :text , :right  and :distractor’ unless @right
		@distractor = args[:distractor]
			raise ArgumentError, ‘Specify :text , :right  and :distractor’ unless @distractor

		if args[:id]
			@id = args[:id]
		else
			args[:id] = @@count
			@@count += 1
		end
 	end
	  def to_html
	    options = @distractor+[@right]
	    options = options.shuffle
	    s = ' '  
	    options.each do |option|
	    s += %Q{<input type =”radio” value “#{option}” class "simple_choice" name = "0"> #{option}\n}
	    #{@text}<br/>\n#{@s}\n"
	    

  	  end

	if __FILE__ == $0 then
		qq = Question::SimpleChoice.new(text: 'x*8=48', right: 6 , distractor: [7,8,9])
		puts qq.to_html
	end 
	
	puts "qq.id = " +Question::SimpleChoice.id
end
